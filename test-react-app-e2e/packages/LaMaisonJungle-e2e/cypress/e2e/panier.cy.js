describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })
  it('passes', () => {

    //au lancement de l'application
    cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .should('exist')//on vérifie son existence
      .should('be.visible')//on vérifie qu'il est visble au lancement de l'application

    //au lancement de l'application
    cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .find('button')//on cherche un élement bouton
      .should('exist')//on vérifie qu'il est présent
      .should('be.visible')//on vérifie qu'il est visible 
      .contains('Fermer')//on vérifie que le bouton contient l'indication 'Fermer'

    //au lancement de l'application
    cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'


    //on va tester la fermeture et l'ouverture du panier
    cy.get('.lmj-layout-inner')
      .children('.lmj-cart')
      .find('button:first')
      .should('exist')
      .should('be.visible')
      .click() //test la fermeture du panier
      .click() //test l'ouverture du panier

      //on va tester si il y a des articles au panier
      cy.get('.lmj-layout-inner')
      .children('.lmj-cart')
      .children('div')
      .children('ul')
      .children('div')
      .should('exist')

      //on va tester de vider le panier
      cy.get('.lmj-layout-inner')
      .children('.lmj-cart')
      .children('div')
      .find('button:first')
      .should('exist')
      .should('be.visible')
      .click() //vide le panier

      //mtn que panier vide > tester si les articles ont été supp du panier
       cy.get('.lmj-layout-inner')
       .children('.lmj-cart')
       .children('div')
       .contains('Votre panier est vide')
  })
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter un article au panier', () =>{

    //Panier vide avant ajout d'un article
    it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter une plante (Monstera) au panier', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button:first')//on cherche le premier bouton
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('monstera 15€ x 1')//on vérifie qu'il contient l'intitulé 'monstera 15€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :15€')//on vérifie qu'il contient l'intitulé 'Total :15€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'
    })
  })  
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter un article au panier', () =>{

     //Panier vide avant ajout d'un article
     it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter une plante (Ficus Lyrata) au panier', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(1)//le deuxieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('ficus lyrata 16€ x 1')//on vérifie qu'il contient l'intitulé 'ficus lyrata 16€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :16€')//on vérifie qu'il contient l'intitulé 'Total :16€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'
    })
  })  
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter un article au panier', () =>{

     //Panier vide avant ajout d'un article
     it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter une plante (Menthe) au panier', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(8)//le neuvieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('menthe 4€ x 1')//on vérifie qu'il contient l'intitulé 'menthe 4€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :4€')//on vérifie qu'il contient l'intitulé 'Total :4€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'
    })
  })  
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter de deux mêmes articles au panier', () =>{

     //Panier vide avant ajout d'un article
     it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter deux plantes (Olivier) au panier', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(4)//le cinquieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 1')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :25€')//on vérifie qu'il contient l'intitulé 'Total :25€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'


      //ajout du deuxieme article


      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(4)//le cinquieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 2')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 2' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :50€')//on vérifie qu'il contient l'intitulé 'Total :50€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'
    })
  })  
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter un article au panier change le head title', () =>{

    //Panier vide avant ajout d'un article
    it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    //Message head title correspondant à un panier vide
    it('Vérifier que l\'entête du site correspond à un panier vide', () => {
      cy.title()//on prend le titre du site
      .should('eq', 'LMJ: 0€ d\'achats')//qui doit contenir le texte 'LMJ: 0€ d'achats'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter une plante (Monstera) au panier et modifier l\'entête du site', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button:first')//on cherche le premier bouton
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('monstera 15€ x 1')//on vérifie qu'il contient l'intitulé 'monstera 15€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :15€')//on vérifie qu'il contient l'intitulé 'Total :15€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'

      cy.title()//on prend le titre du site
      .should('eq', 'LMJ: 15€ d\'achats')//qui doit contenir le texte 'LMJ: 15€ d'achats' correspondant au total du panier
    })   
  })  
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter de deux mêmes articles au panier change le head title', () =>{

     //Panier vide avant ajout d'un article
     it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    //Message head title correspondant à un panier vide
    it('Vérifier que l\'entête du site correspond à un panier vide', () => {
      cy.title()//on prend le titre du site
      .should('eq', 'LMJ: 0€ d\'achats')//qui doit contenir le texte 'LMJ: 0€ d'achats'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter deux plantes (Olivier) au panier et modifier l\'entête du site', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(4)//le cinquieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 1')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :25€')//on vérifie qu'il contient l'intitulé 'Total :25€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'


      //ajout du deuxieme article


      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(4)//le cinquieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 2')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 2' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :50€')//on vérifie qu'il contient l'intitulé 'Total :50€' correspondant au total de panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'

      cy.title()//on prend le titre du site
      .should('eq', 'LMJ: 50€ d\'achats')//qui doit contenir le texte 'LMJ: 15€ d'achats' correspondant au total du panier
    })
  })  
})

describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })

  describe('Ajouter de deux mêmes articles au panier et un autre article modifie l\'entête du site', () =>{

     //Panier vide avant ajout d'un article
     it('Vérifier que le panier est vide', () => {
      cy.get('.lmj-layout-inner')//on prend la fenetre principale
      .children('.lmj-cart')//on cherche le panier
      .children('div')//on cherche un élément div
      .contains('Votre panier est vide')//qui contient le texte 'Votre panier est vide'
    })

    it('Vérifier que l\'entête du site correspond à un panier vide', () => {
      cy.title()//on prend le titre du site
      .should('eq', 'LMJ: 0€ d\'achats')//qui doit contenir le texte 'LMJ: 0€ d'achats'
    })

    //Les produits sont bien tous affichés
    it('Vérifier la présence de tous les produits', () => {
      cy.get('.lmj-categories')//on prend la liste des catégories
      .find('select')//on cherche la liste de selection des articles
      .select('---')//on selectionne toutes les catégories
      .should('exist')//on vérifie que cette sélection existe
      .should('be.visible')//on vérifie qu'elle est affichée
      .get('.lmj-plant-list')//on prend la liste des articles
      .children()//on prend tous les éléments enfants
      .should('have.length', 9)//on vérifie qu'on a bien 9 articles d'affichés
    })

    it('Cliquer pour ajouter deux plantes (Olivier) au panier + une autre plante (Cactus) et que ça modifie correctement l\'entête du site', () => {      
      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(4)//le cinquieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 1')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :25€')//on vérifie qu'il contient l'intitulé 'Total :25€' correspondant au total du panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'


      //ajout du deuxieme article

      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(4)//le cinquieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'
      
      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 2')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 2' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :50€')//on vérifie qu'il contient l'intitulé 'Total :50€' correspondant au total de panier


      //ajout du troisieme article

      cy.get('.lmj-plant-list')//on prend la liste des plantes
      .find('button')//on cherche un bouton
      .eq(5)//le sixieme
      .contains('Ajouter')//on vérifie qu'il contient l'intitulé 'Ajouter'
      .click()//on clique dessus

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h2')//on cherche un titre 2
      .contains('Panier')//avec l'intitulé 'Panier'

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('olivier 25€ x 2')//on vérifie qu'il contient l'intitulé 'olivier 25€ x 2' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('ul')//on cherche un élément liste
      .children('div')//dans l'élément div enfant
      .contains('cactus 6€ x 1')//on vérifie qu'il contient l'intitulé 'cactus 6€ x 1' correspondant au nom de la plante ajoutée, son prix et la quantité présente dans le panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('h3')//on cherche un élément titre 3
      .contains('Total :56€')//on vérifie qu'il contient l'intitulé 'Total :56€' correspondant au total de panier

      //Vérification de l'entête du site

      cy.title()//on prend le titre du site
      .should('eq', 'LMJ: 56€ d\'achats')//qui doit contenir le texte 'LMJ: 56€ d'achats' correspondant au total du panier

      cy.get('.lmj-cart')//on prend le panier
      .children('div')//dans l'élément div enfant
      .find('button')//on cherche un élément bouton
      .contains('Vider le panier')//on vérifie qu'il contient l'intitulé 'Vider le panier'
    })
  })  
})