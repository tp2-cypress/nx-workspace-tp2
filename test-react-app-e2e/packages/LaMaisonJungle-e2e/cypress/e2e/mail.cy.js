describe('empty spec', () => {
    it('passes', () => {
        cy.visit('/')
        cy.location('pathname', { timeout: 10000 })
        .should('eq', '/')

        cy.get('footer')
        .children('input')
        .should('exist')
        .should('be.visible')
        .type('axel.gilbert28gmail.com')
        cy.on('window:alert', (txt) => {
        expect(txt).to.contains("Attention, il n'y a pas d'@, ceci n'est pas une adresse valide 😥")
        })
    })
})

describe('empty spec', () => {
    it('passes', () => {
      cy.visit('/')
      cy.location('pathname', { timeout: 10000 })
        .should('eq', '/')
  
      cy.get('footer')
        .children('input')
        .should('exist')
        .should('be.visible')
        .type('axel.gilbert@28gmail.com')
    })
})